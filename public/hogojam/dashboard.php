<?php
//include auth_session.php file on all user panel pages
include("auth_session.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Dashboard - Client area</title>
    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="../style.css">
</head>
<body>
<nav class="navbar">
      <div class="max-width">
        <div class="logo"><a href="../index.html">Hogo<span>Team.</span></a></div>
        <ul class="menu">
          <li><a href="../index.html">Domů</a></li>
          <li><a href="../pffd/index.html">Place For Fun</a></li>
          <li><a href="#">Hogo SMP</a></li>
          <li><a href="#">O mě</a></li>
          <li><a href="./hogojam/login.php">Hogo JAM</a></li>
        </ul>
        <div class="menu-btn">
          <i class="fas fa-bars"></i>
        </div>
      </div>
    </nav>
    <div class="form">
        <p>Hey, <?php echo $_SESSION['username']; ?>!</p>
        <p>You are now user dashboard page.</p>
        <p><a href="logout.php">Logout</a></p>
    </div>
</body>
</html>
